﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PassionProject.Models
{
    public class Game
    {
        [Key, ScaffoldColumn(false)]
        public int GameID { get; set; }

        [Required, StringLength(255), Display(Name = "Game Name")]
        public string GameName { get; set; }

        [StringLength(255), Display(Name = "Description")]
        public string GameDescription { get; set; }

        public int HasLogo { get; set; }

        [StringLength(255), Display(Name = "Game Logo")]
        public string GameLogo { get; set; }

        public virtual ICollection<Category> Categories { get; set; }
    }
}