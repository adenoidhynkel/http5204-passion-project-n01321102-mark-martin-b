﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PassionProject.Models
{
    public class Guild
    {
        [Key, ScaffoldColumn(false)]
        public int GuildId { get; set; }

        [Required, StringLength(255), Display(Name = "Guild Name")]
        public string GuildName { get; set; }

        [StringLength(255), Display(Name = "Description")]
        public string GuildDescription { get; set; }

        public int HasEmblem { get; set; }

        [StringLength(255), Display(Name = "Guild Emblem")]
        public string GuildEmblem{ get; set; }

        public virtual Game game { get; set; }
        public virtual ICollection<Application> Application { get; set; }
    }
}