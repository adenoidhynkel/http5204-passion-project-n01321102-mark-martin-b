﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PassionProject.Models
{
    public class Player
    {
        [Key, ScaffoldColumn(false)]
        public int PlayerID { get; set; }

        [Required, StringLength(255), Display(Name = "First Name")]
        public string PlayerFName { get; set; }

        [Required, StringLength(255), Display(Name = "Last Name")]
        public string PlayerLName { get; set; }

        [StringLength(255), Display(Name = "Discord Tag Name")]
        public string PlayerDiscord { get; set; }

        public int HasAvatar { get; set; }

        [StringLength(255), Display(Name = "Avatar")]
        public string PlayerAvatar { get; set; }

        public virtual ICollection<Game> Games { get; set; }

        // To Do : create a friendship Module
        // public virtual ICollection<Friendship> Friends { get; set; }
    }
}