﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data.Sql;
using PassionProject.Models;
using System.Diagnostics;
using System.Data.SqlClient;

namespace PassionProject.Controllers
{
    public class PlayerController : Controller
    {
        private TeamBuilderContext database = new TeamBuilderContext();
        public ActionResult Index()
        {
            return RedirectToAction("PlayerList");
        }

        public ActionResult PlayerList()
        {
            return View(database.Players.ToList());
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult PlayerProfile(int id)
        {
            string query = "select * from players where playerid = @id";
            Debug.WriteLine(query);
            return View(database.Players.SqlQuery(query, new SqlParameter("@id", id)).FirstOrDefault());
        }

        // Register an Account
        [HttpPost]
        public ActionResult NewPlayer(string PlayerFName_New, string PlayerLName_New, string PlayerDiscord_New)
        {
            return View();
        }



    }
}